import java.nio.file.DirectoryStream.Filter;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.print.DocFlavor.STRING;
import javax.swing.Spring;


public class FP_Functional_Exercises{
    public static void main(String[] args){
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

        List<String> courses = List.of("Spring", "Spring Boot", "API",
            "Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");

            printEvenNumbersInListFunctional(numbers);
            
            printEventCoursesInListFunctional(courses);

            printEventCourseSInListFunctional(courses);

            printEventCInListFunctional(courses);

            printSquaresOfEvenNumbersInListFunctional(numbers);

            printCountEvenNumbersInListFunctional(courses);
    }

    private static void print(int number){
        System.out.print(number + ", ");
    }

    private static void printC(String courses){
        System.out.print(courses + ", ");
    }

    private static boolean isEvent(int number){
        return (number % 2 == 1);
    }

    //Ejercicio 1
    private static void printEvenNumbersInListFunctional(List<Integer> numbers){
        System.out.println("Exercise 1");
        numbers.stream().
        filter(FP_Functional_Exercises::isEvent)
        .forEach(FP_Functional_Exercises::print);
        System.out.println("\n");
    }
    //Ejercicio 2
    private static void printEventCoursesInListFunctional(List<String> courses){
        System.out.println("Exercise 2");
        courses.stream()
            .forEach(FP_Functional_Exercises::printC);
        System.out.println("\n");
    }
    //Ejercicio 3
    private static void printEventCourseSInListFunctional(List<String> courses){
        System.out.println("Exercise 3");
        courses.stream()
            .filter(c -> c.contains("Spring")).
            forEach(FP_Functional_Exercises::printC);
        System.out.println("\n");
    }
    //Ejercicio 4
    private static void printEventCInListFunctional(List<String> courses){
        System.out.println("Exercise 4");
        courses.stream()
            .filter(c -> c.length() >= 4).
            forEach(FP_Functional_Exercises::printC);
        System.out.println("\n");
    }
    //Ejercicio 5
    private static void printSquaresOfEvenNumbersInListFunctional(List<Integer> numbers){
        System.out.println("Exercise 5");
        numbers.stream()
            .filter(number -> number % 2 == 1)
            .map(number -> number * number *number)
            .forEach(FP_Functional_Exercises::print);
            System.out.println("\n");
    }
    //Ejercicio 6
    private static void printCountEvenNumbersInListFunctional(List<String> courses){
        System.out.println("Exercise 6");
        courses.stream()
            .map(course -> course + " = " + course.length())
            .forEach(FP_Functional_Exercises::printC);
        System.out.println("\n");
    }
}